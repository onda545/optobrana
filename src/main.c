
#include <stdbool.h>
#include <stdio.h>
#include <stm8s.h>
#include "main.h"
#include "max7219.h"
#include "milis.h"

#define SNIMAC GPIOD, GPIO_PIN_6 

#define DIN_PORT GPIOB
#define DIN_PIN GPIO_PIN_4
#define CS_PORT GPIOB
#define CS_PIN GPIO_PIN_3
#define CLK_PORT GPIOB
#define CLK_PIN GPIO_PIN_2

#define LED_RED GPIOE, GPIO_PIN_0
#define LED_GRN GPIOD, GPIO_PIN_5

void init(void) {
    CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // taktovani MCU na 16MHz

    // Inicializace 7segmentoveho displeje
    GPIO_Init(DIN_PORT, DIN_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(CS_PORT, CS_PIN, GPIO_MODE_OUT_PP_HIGH_SLOW);
    GPIO_Init(CLK_PORT, CLK_PIN, GPIO_MODE_OUT_PP_LOW_SLOW);

    GPIO_Init(SNIMAC, GPIO_MODE_IN_PU_NO_IT); // Optobrana

    // Led diody
    GPIO_Init(LED_RED, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_Init(LED_GRN, GPIO_MODE_OUT_PP_LOW_SLOW);

    init_milis();
}

// zobrazeni cisla na digitu displeje
void display(uint8_t address, uint8_t data) {
    uint8_t mask;
    LOW(CS); // začátek přenosu

    // adresa
    mask = 0b10000000;
    while (mask) {
        if (address & mask) {
            HIGH(DIN);
        } else {
            LOW(DIN);
        }
        HIGH(CLK);
        mask = mask >> 1;
        LOW(CLK);
    }
    
    // data
    mask = 0b10000000;
    while (mask) {
        if (data & mask) {
            HIGH(DIN);
        } else {
            LOW(DIN);
        }
        HIGH(CLK);
        mask = mask >> 1;
        LOW(CLK);
    }

    HIGH(CS); // konec přenosu
}

int main(void) {
    uint32_t time = 0, time_2 = 0, timer_100ms = 0; // casovace, pocet odpocitanych 100ms 
    bool snimani = false, casovani = false; // jestli optobrana naposledy snimala, jestli prave probiha casovani

    init(); // Inicializace

    // Nastaveni displeje
    display(DECODE_MODE, 0b11111111);
    display(SCAN_LIMIT, 7);
    display(INTENSITY, 1);
    display(DISPLAY_TEST, DISPLAY_TEST_OFF);
    display(SHUTDOWN, SHUTDOWN_ON);
        
    while (1) {
        // pricitani 100ms
        if ((milis() - time_2 > 100) && (casovani))
        {
            time_2 = milis();
            timer_100ms++;
        }

        if (milis() - time > 20) {
            time = milis();

            // zobrazeni na displeji
            display(DIGIT4, (timer_100ms / (10*60*10)));
            display(DIGIT3, (timer_100ms % (10*60*10)) / (10*60)+ 0b10000000);
            display(DIGIT2, (timer_100ms % (10*60)) / (10*10));
            display(DIGIT1, ((timer_100ms % (10*10)) / 10) + 0b10000000);
            display(DIGIT0, timer_100ms % 10);
            
            // snimani optobrany
            if (GPIO_ReadInputPin(SNIMAC))
            {
                // prepnuti casovace
                if(!snimani)
                {
                    casovani = !casovani;

                    // resetovani casovace
                    if (casovani)
                    {
                        time_2 = milis();
                        timer_100ms = 0;
                    }
                }
                snimani = true;
            }
            else
            {
                snimani = false;
            }

            // Rozsveceni led diod
            if (casovani)
            {
                GPIO_WriteLow(LED_RED);
                GPIO_WriteHigh(LED_GRN);
            }
            else
            {
                GPIO_WriteHigh(LED_RED);
                GPIO_WriteLow(LED_GRN);
            } 
        }
    } 
}

/*-------------------------------  Assert -----------------------------------*/
#include "assert.h"
